let http = require("http");

let port = 4000;

http.createServer(function(request, response){

	// The HTTP method of the incoming request can be accessed via the "method" property of the "request" parameter
	// The method "GET" means that we will be retrieving or reading information
	if(request.url == "/items" && request.method == "GET"){
		// Requests the "/items" path and "GETS" information
		response.writeHead(200, {"Content-Type": "text/plain"});
		// Ends the response processs
		response.end("Data retrieved from the database");
	} else if (request.url == "/items" && request.method == "POST"){
		// Requests the "/items" path and "SENDS" information
		response.writeHead(200, { "Content-Type": 'text/plain'});
		response.end("Data to be sent to the database")
	}

}).listen(port);

console.log("Running at localhost: 4000");