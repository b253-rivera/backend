// alert("Bulaga")

/*
	OBJECTS
		- is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities.
*/


	// Object Literals
	/*
		- one of the methods in creating objects

		Syntax:
			let objectName = {
				keyA/propertyA: valueA,
				keyB/propertyB: valueB
			}
	*/
let cellphone = {
	name: "Nokia 3210",
	manufactureDate: 1999
};

console.log("Result from creating object using Object Literals:");
console.log(cellphone);
console.log(typeof cellphone);

// Creating Objects Using a Constructor Function (Object Constructor)
/*
	Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object. 

	Note: when creating an object using constructor function the naming convention will be on PascalCase

	Syntax: 
		function ObjectName(param_valueA, param_valueB){
			this.keyA = valueA,
			this.keyB = valueB
		}

		let variableName = new function ObjectName(valueA, valueB)
		console.log(variableName);

		Note: Do not forget the "new" keyword/reserved word when creating a new object
*/

// function declaration for object (Constructor Function/Object Constructor)
function Laptop(brand, manufactureDate){
	this.brand = brand,
	this.manufactureDate = manufactureDate
}

let laptopOwen = new Laptop("MacBook air", [2020, 2021]);
console.log("Result of creating objects using Constructor Function: ");
console.log(laptopOwen);

let laptop = Laptop("MacBook air", [2020, 2021]);
console.log("Result of creating objects without new keyword: ");
console.log(laptop);

// Create empty objects as placeholder
let computer = {};
let myComputer = new Object();
console.log(computer);
console.log(myComputer);

/*
	Mini Activity:
		- Create an object constructor function to produce 2 objects with 3 key-value pairs.
		- Log the 2 new objects in the console and send SS in our batch hangout.

*/

// function Coins(lima, sampusingkwenta, tanong){
// 	this.lima = lima,
// 	this.sampusingkwenta = sampusingkwenta,
// 	this.tanong = tanong
// }

// let coin = new Coins("5", "hundred pipty pesos", "gusto mo lima?");
// console.log(coin);

// let token = new Coins("toms world", "timezone", "ewan ko");
// console.log(token);



// let myPhone = new Phone("Samsung", "Galaxy Z Flip4", 2022);
// console.log(myPhone)

let laptopJanie = new Laptop("Lenovo", 2008);
console.log("Result of creating objects using Constructor Function:")
console.log(laptopJanie);

// Accessing Object Properties
	//using dot notation
console.log("Result from dot notation: " + laptopJanie.brand);
	// using dot notation
console.log("Result from square bracket notation: " + laptopJanie["brand"])

let deviceArr = [laptopJanie, laptopOwen];
console.log(deviceArr);
console.log(deviceArr[0].brand);
console.log(deviceArr[0]["manufactureDate"]);

// Initializing/Adding/Deleting/Reassigning Object Properties
// CRUD OPERATIONS

// Initializing Object
let car = {};
console.log(car);

// Adding Object Properties
/*
		    - While using the square bracket will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accesssed using the square bracket notation
		    - This also makes names of object properties to not follow commonly used naming conventions for them
*/
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);

car["manufacture date"] = 2019;
console.log(car["manufactureDate"]);
console.log(car["ManufactureDate"]);
console.log(car.manufactureDate);
console.log("Results from adding properties using square bracket notation");
console.log(car);

// Deleting Object Properties
delete car["manufacture date"];
console.log("Result from deleting properties:");
console.log(car)

car.name = "Tesla";
console.log("Result from reassigning property:");
console.log(car);

// Object Methods
/*
	This method is a function which is stored in an object property. Ther are also functions and one of the key difference that they have is that methods are functions related to a specific object.
*/



let person = {
	name: "Pedro",
	age: 25,
	talk: function(){
		console.log("Hello! Ako si " + this.name)
	}
};

console.log(person)
console.log("Result from object method:");
person.talk();

person.walk = function(){
	console.log(this.name + " have walked 25 steps forward.")
}

person.walk();

//===================================

let friend = {
	firstName: "Maria",
	lastName: "Dela Cruz",
	address:{
		city: "Austin, Texas",
		country: "US"
	},
	emails: ["maria143@mail.com", "maria_bosxz@mail.com"],
	introduce: function(){
		console.log("Hello! My name is " + this.firstName + " " + this.lastName + "." + "I live in " + this.address.city + "," + this.address.country + ".")
	}
}

friend.introduce();

//====================================
let myPokemon = {
	name: "Charizard",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This pokemon tackled targetPokemon");
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	},
	faint: function(){
		console.log("Pokemon fainted");
	}
}

console.log(myPokemon);

// Creating an object constructor instead will help with this process
function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + target.health);
	};
	this.faint = function(){
		console.log(this.name + "fainted");
	}
}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let charmander = new Pokemon("Charmander", 8)

pikachu.tackle(charmander);