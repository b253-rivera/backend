// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object


let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends:{
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"],
	},
	talk: function(){
		console.log(this.pokemon[0] + " I choose you!")
	}
}

console.log(trainer)

console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:")
trainer.talk();

function Pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = health - attack;
	this.attack = attack;

	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name)
		console.log(target.name + "'s health is now reduced to " + health)
	}

	this.faint = function(){
		console.log(this.name + " fainted")
	}
}

let pikachu = new Pokemon("Pikachu", 12, 24, 12, )
console.log(pikachu);

let geodude = new Pokemon("Geodude", 8, 16, 8)
console.log(geodude);

let mewtwo = new Pokemon("Mewtwo", 100, 200, 100)
console.log(mewtwo);

geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);

geodude.faint()


//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}