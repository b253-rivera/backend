 // Comments:

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/* 

	There are two types of comments:
		1. Single-Line Comment - denoted by two slashes
		2. Multi-Line Comment - denoted by a forward slash and two asterisks
*/

console.log("Hello Batch 253!");

/*
		Important Note:
			- The "script" tag is commonly placed at the bottom of the 
			HTML file right before the closing "body" tag.
			- The reason for this is because Javascript's main function in frontend 
			development is to make our websites and applications interactive.
			- In order to achieve this, JavaScript selects/targets specific 
			HTML elements in our application and performs a certain output.
			- It is added last to allow all HTML and CSS resources to load first 
			before applying any JavaSc
			- It is added last to allow all HTML and CSS resources to load first 
			before applying any JavaScript code to run.
			- Placing the "script" tag at the top the the file might result in errors 
			because since the HTML elements have not yet been loaded when the 
			JavaScript loads, it does not have any valid HTML elements to target/select.
*/
/*
	Syntax - this is the grammar of the language
	Statements - this is the commands or instructions for the computer
*/

// [SECTION] Variables

// IT is used to contain data
// Any information that is used by an application is stored in what we call a memory

// When we create variables, certain portions of a device's memory is given a "name" that we call variables

// This makes it easier for us associate information stored in our devices to 
// actual "names about information"
//The names of a variables is called "identifier"

// Declaring variables

// Declaring variables - tells our devices that a variable name is created and is ready to store data
// Declaring a variable without giving it a value will automatically assign it 
//with the value of "undefined", meaning the variable's value was "not defined".

/*
	Syntax:
		let/const variableName
*/
// let is a keyword that is usually used in declaring a variable.

let feelings = true;

// console.log(feelings);

// let hello;

/*
    Guides in writing variables:
        1. Use the 'let' keyword followed by the variable name of your choosing and use the assignment operator (=) to assign a value.
        2. Variable names should start with a lowercase character, use camelCase for multiple words.
        3. For constant variables, use the 'const' keyword.
        4. Variable names should be indicative (or descriptive) of the value being stored to avoid confusion.
Best practices in naming variables:

		1. When naming variables, it is important to create variables that are descriptive and indicative of the data it contains.

			let firstName = "Michael"; - good variable name
			let pokemon = 25000; - bad variable name
		2. When naming variables, it is better to start with a lowercase letter. We usually avoid creating variable names that starts with capital letters. Because there are several keywords in JS that start in capital letter.

			let FirstName = "Michael"; - bad variable name
			let firstName = "Michael"; - good variable name
		3. Do not add spaces to your variable names. Use camelCase for multiple words, or underscores.

			let first name = "Mike";

		camelCase is when we have first word in small caps and the next word added without space but is capitalized:

			lastName emailAddress mobileNumber
		Underscores sample:

		let product_description = "lorem ipsum"
		let product_id = "250000ea1000"

*/

// Initializing variables - the instance when a variable is given it's initial/starting value
// Syntax
// let/const variableName = value;


let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;

/* let variable cannot be re-declared within its scope.*/
let friend = "Owen";
//let/const friend; error: identifier friend has already been declared
friend = "Blessy"; // re-assigned/re-declared variable;
friend = "Casper";
console.log(friend);

//while const cannot be updated or re-declared
// Values of constants cannot be changed and will simply return an error
// interest = 3.14159265; TypeError Assignment to const variable
console.log(interest);

// This is a declaration of variable sinec it has no value yet
let supplier;
// This is still an initialization of variable since this is the first time 
//assigning its value
supplier = "John Smith Trading";
// This is considered as reassignment because its initial 
//value was already declared
supplier = "Zuitt Store";
console.log(supplier);

const pi = 3.14159265;
console.log(pi);

//There are issues associated with variables declared with var, regarding with hoisting.
//In terms of variables and constants, keyword var is hoisted and let and const does not allow hoisting.
//Hoisting is JavaScript's default behavior of moving declarations to the top.
var pokemon = "Pikachu"
console.log(pokemon);

// a = 5;
// console.log(a);
// let a;

// let outerVar = "hello from the outside";

// {
// 	let innerVar = "hello from inside"
// };

// console.log(outerVar);
// console.log(innerVar);

console.log(supplier, productPrice, productName);

// [SECTION] Data Types

// Strings
// Strings are a series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strings in JavaScript can be written using either a single (') or double (") quote
// In other programming languages, only the double quotes can be used for creating strings

let country = 'Philippines';
let province = "Metro Manila";

// Concatenating strings
// Multiple string values can be 
//combined to create a single string using the "+" symbol

let fullAddress = province + ',' + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

/*
	Add your name in a console log w/o using a variable but a concatenation

	My name is Janie. I live in the Philippines.
*/

console.log("My name is Emvir." + " " + greeting);

// (\) escape character - in combination with other characters 
//can produce different effects
// "\n" refers to creating a new line in between texts
let mailAddress = "Metro Manila\n\nPhilippines";
console.log(mailAddress);

let message = "John's employees went home early today";
console.log(message);
message = 'John\'s employees went home early today';
console.log(message);

message = "John said to his employees \"Hello!\"";
console.log(message);

// NUMBERS/integer/ whole number
let headcount = 26;
console.log(headcount);

//decimal number/float/fractions
let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

// data type number will become a string once it is combined/concatenated w/ a string
console.log("John's grade last quarter is " + grade);

// BOOLEAN values are normally used to store values
//relating to the state of certain things
// This will be useful in further discussions about 
//creating logic to make our application respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("inGoodConduct: " + inGoodConduct);

// ARRAYS
// Arrays are a special kind of data type that's used to store
// multiple values
// Arrays can store different data types BUT is normally used to store SIMILAR data types

/*
		Syntax:
		 let/const arrayName = [elementA, elementB, elementC, ...];
*/

let grades = [98.7, 92.1, 90.2, 94.6]
console.log(grades);

// different data types
// storing different data types inside an array is not 
//recommended because it will not make sense to in the context of programming
let details = ["John", "Smith", 32, true];
console.log(details);


// OBJECTS
// Objects are another special kind of data type that's used to mimic real world objects/items
// They're used to create complex data that 
//contains pieces of information that are relevant to each other
// Every individual piece of information is called a property of the object


/*

	Syntax:
		let/const objectName

*/
let person = {
	fullName: "Bilbo Baggins",
	age: 90,
	isMarried: false,
	contact:["09123456789", "1234 5678"],
	address: {
		houseNumber: '123',
		city: "Shire"
	}
};

console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};
console.log(myGrades);

//typeof operator is used to determine the type data or the value
//of a variable. Its output is a string
console.log(typeof myGrades);
// Note: Array is a special type of object with methods and functions to manipulate it
console.log(typeof grades);

const anime = ["Madoka Magical", "Parasyte", "One Piece"];
// anime = ["bungou stray dogs"];
anime[0] = "kimetsu no yaiba";
console.log(anime);
/*===============================================*/
// NULL
// It is used to intentionally express the absence of a 
//value in a variable declaration/initialization
// null simply means that a data type was assigned 
//to a variable but it does not hold any value/amount or is nullified
/*===============================================*/
let spouse = null, myNumber = 0, myString = '';
// Using null compared to a 0 value and an empty string is 
//much better for readability purposes
// null is also considered as a data 
//type of it's own compared to 0 which is a data type of a 
//number and single quotes which are a data type of a string
console.log(spouse);
console.log(myNumber);
console.log(myString);
/*==================================================*/

// UNDEFINED
// Represents the state of a variable that has been declared but without an
// assigned value
console.log("Feeling is " + feelings);

// Undefined vs Null
// One clear difference between undefined and null is that for undefined, 
//a variable was created but was not provided a value
// null means that a variable was created and was assigned 
//a value that does not hold any value/amount
// Certain processes in programming would often return a "null" 
//value when certain tasks results to nothing

