// console.log("Hello");

// Javascript Synchronous vs Asynchronous
// Javascript is by default is synchronous meaning that only one statement is executed at a time

// This can be proven when a statement has an error, javascript will not proceed with the next statement
console.log("Hello World");
// conosle.log("Hello Again");
console.log("Guten Morgen");

// console.log("Hello World");
// for(let i = 0; i <= 1500; i++){
// 	console.log(i);
// };

console.log("Hello Again");

// Asynchronous means that we can proceed to execute other statements, while time consuming code is running in the background

// Getting All Posts

// The Fetch API allows you to asynchronously request for a resource (data)
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and it's resulting value
/*
		Syntax:
			fetch("url")
*/

console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

/*
	Syntax: 
		fetch("url")
		     .then((response) => {});
*/

// Retrieves all posts following the Rest API (retrieve, /posts, GET)
// By using the then method we can now check for the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")
// The "fetch" method will return a "promise" that resolves to a "Response object"
// The "then" method captures the "Reponse" object and returns another "promise" which will eventually be "resolved" or "rejected"
	.then((response) => response.json()

	).then((json) => console.log(json));
	

fetch("https://jsonplaceholder.typicode.com/posts")
// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application
	.then((response) => {
		return response.json()
		// Print the converted JSON value from the "fetch" request
		// Using multiple "then" methods creates a "promise chain"
	}).then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
// Used in functions to indicate which portions of code should be waited for
// Creates an asynchronous function
async function fetchData(){

	// waits for the "fetch" method to complete then stores the value in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");

	// Result returned by fetch returns a promise
	console.log(result) //1
	// The returned "response" is an object
	console.log(typeof result);
	// We cannot access the content of the "response" by directly accessing its body property
	console.log(result.body);

	// Converts the data from the "response" object as JSON
	let json = await (result.json()); //2
	// Print out the content of the "response" object
	console.log(json); //3
}
fetchData();

// Getting specific post 

// Retrieves a specific post following the Rest API (retrieve, /posts/:id, GET)

fetch("https://jsonplaceholder.typicode.com/posts/1")
	.then(response => response.json())
	.then(json => console.log(json));

// Creating a post
/*
	Syntax:
		fetch("url" {options})
			.then((response) => {})
			.then(response => ());
			

*/

fetch("https://jsonplaceholder.typicode.com/posts/", {
	method: "POST",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "New post",
		body: "Hello World!",
		userId: 1
	})
}).then(response => response.json())
	.then(json => console.log(json));

// Updating a post
// Updates a specific post following the Rest API (update, /posts/:id, PUT)

fetch("https://jsonplaceholder.typicode.com/posts/2", {
	method: "PUT",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		id: 1,
		title: "kobe",
		body: "bryant!",
		userId: 1
	})
}).then(response => response.json())
	.then(json => console.log(json));

//==========================================================
// Updates a specific post following the Rest API (update, /posts/:id, Patch)
// The difference between PUT and PATCH is the number of properties being changed
// PUT is used to update the whole object
// PATCH is used to update a single/several properties
fetch("https://jsonplaceholder.typicode.com/posts/2", {
	method: "PATCH",
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "Corrected post"

	})
}).then(response => response.json())
	.then(json => console.log(json));

// Deleting a post
// Deleting a specific post following the Rest API (delete, /posts/:id, DELETE)

fetch("https://jsonplaceholder.typicode.com/posts/2", {
	method: "DELETE"
});