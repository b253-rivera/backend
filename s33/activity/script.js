
fetch("https://jsonplaceholder.typicode.com/todos/1")
	.then((response) => response.json())
	.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos")
	.then((response) => response.json())
	.then((json) => {

		let list = json.map((todo => {
			return todo.title;
		}))

		console.log([list]);
	});