const express = require("express");
const router = express.Router();


const userController = require("../controllers/userController")
const auth = require("../auth");
const User = require("../models/User");
const Course = require("../models/Course");

// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req,res) => {

	// The full route to access this is "http://localhost:4000/users/checkEmail" where the "/users" was defined in our "index.js" file
	// The "then" method uses the result from the controller function and sends it back to the frontend application via the "res.send" method
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});

// Route for user registration

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});

router.post("/login", (req,res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
})
// Route for retrieving user details
// router.post("/details", (req,res) => {

// 	// userController.getProfile(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

// 	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
// 	const userData = auth.decode(req.headers.authorization)

// 	userController.getProfile({ userId : userData.id}).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
// });

router.get("/details", (req,res) => {

	// Provides the user ID for the getProfile controller method
	// userController.getProfile(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization)

	userController.getProfile({ userId : userData.id}).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err))
});

// Route to enroll user to a course

// router.post("/enroll", (req, res) => {

// 	let data = {
// 		userId : req.body.userId,
// 		courseId : req.body.courseId
// 	}

// 	userController.enroll(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));
// })

router.post("/enroll", auth.verify, (req, res) => {

    const userVerif = auth.decode(req.headers.authorization);
    console.log(userVerif);

    if (userVerif.isAdmin) {

        res.send(false);

    } else {

        let data = {
            userId: userVerif.id,
            isAdmin : userVerif.isAdmin,
            courseId: req.body.courseId
        }

        userController.enroll(data).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

        console.log(userVerif);
    }
})

// Allows us to export the router object that will be accessed in our index
module.exports = router;
