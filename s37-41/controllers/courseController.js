const Course = require("../models/Course");
const secret = "CourseBookingAPI"

// Create a new course
		/*
			Steps:
			1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
			2. Save the new User to the database
		*/
// Create a new course
		/*
			Steps:
			1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
			2. Save the new User to the database
		*/

// module.exports.addCourse = (reqBody) => {
	
// 	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
// 	// Uses the information from the request body to provide all the necessary information
// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	return newCourse.save().then(course => true)
// 		.catch(err => false);
// }
//================================================Activity
// module.exports.createCourse = (req, adm) => {
// 	if(adm == true){

// 		let createCourse = new Course({
// 		name : course.name,
// 		description : course.description,
// 		price : course.price
// 	});

// 		return createCourse.save().then(course => true).catch(err => false);

// 	} else {
// 		return Promise.reject("User is not an Admin");
// 	}
// };
//==================================================ms janie code
module.exports.addCourse = (reqBody) => {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		});

		return newCourse.save().then(course => true)
			.catch(err => false);
};

// Retrieve all courses
		/*
			Steps:
			1. Retrieve all the courses from the database
		*/
module.exports.getAllCourses = () => {

	return Course.find({}).then(result => result). catch(err => err);
}


module.exports.getAllActive = () => {

	return Course.find({ isActive : true}).then(result => result).catch(err => err);
}

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	}).catch(err => err);
}

// Update a course
		/*
			Steps:
			1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
			2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
		*/
		// Information to update a course will be coming from both the URL parameters and the request body

module.exports.updateCourse = (reqParams, reqBody) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	/*
		Syntax:
			ModelName.findByIdandUpdate(documentId, updatesToBeApplied).then(statement)

			req.params = {courseId: _id}
			req.params.id = _id

	*/
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then(course => true).catch(err => err);
}


//=================activity

// Route to archiving a course
// A "PUT"/"PATCH" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
module.exports.updateAdmin = (reqParams, body) => {

	let updatedAdmin = {isActive: body.isActive}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedAdmin).then(active => true).catch(err => err);
};

