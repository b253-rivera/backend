// console.log("Hello!");

// LOOPS
	// While Loop
	/*
		- A while loop takes in an expression/condition
		- Expressions are any unit of code that can be evaluated to a value
		- If the condition evaluates to true, the statements inside the code block will be executed
		- A statement is a command that the programmer gives to the computer
		- A loop will iterate a certain number of times until an expression/condition is met
		- "Iteration" is the term given to the repetition of statements

		Syntax: 
			while(expression/condition){
				statement;
			}
	*/

let count = 5;

while(count !== 0){

	// The current value of count is printed out
	console.log("While: " + count);
	count--
	console.log(count);
}


/*
	MINI-ACTIVITY

	Create a while loop if "i" variable is less than or equal to 15,
	display "Hi <i>

*/

let i = 5;

while(i <= 0){
	console.log("Hi: " + i);
	i++
}

//======================================================
	// Do-While Loop
	/*
		- A do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

		Syntax:

			do{
				statement
			} while (expression/condition)
	*/

// let number = Number(prompt("Give me a number"));

// do{
// 	console.log("Do While: " + number);


// 	 // Increases the valie of number by 1 after every iteration to stop the loop
// 	// number = number + 1
// 	number += 2
// 	console.log(number);


// } while (number < 10);

//========================================================
		// For Loop
	
	/*
		- A for loop is more flexible than while and do-while loops. It consists of three parts:
		    1. The "initialization" value that will track the progression of the loop.
		    2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
		    3. The "iteration" indicates how to advance the loop.
		Syntax:
			for(initialization/initial value; expression/condition; iteration){
				
			}
	*/

/*
		    - Will create a loop that will start from 0 and end at 20
		    - Every iteration of the loop, the value of count will be checked if it is equal or less than 20
		    - If the value of count is less than or equal to 20 the statement inside of the loop will execute
		    - The value of count will be incremented by one for each iteration
		*/
// for (let n = 0; n <= 20; n++){

// 	// The current value of n is printed out
// 	console.log(n);
// }


let myString = "Iamadeveloper";

// .length property is a number data type
console.log(myString.length);

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

for(let x = 0; x < myString.length; x++){

	console.log(myString[x]);
}

/*
		Mini-Activity

		create a for loop that will use your given name as values and length and count the vowels

*/

let myName = "jAnIeLeNe";
let vowel = 1

for(let x = 0; x < myName.length; x++){

	let letter = myName.charAt(x).toLowerCase();
	if(
		myName[x] == "a" ||
		myName[x] == "e" ||
		myName[x] == "i" || 
		myName[x] == "o" ||
		myName[x] == "u" 
		){
		vowel++;
	}
}

// console.log("Number of vowels: " + myName + "," + vowel);

for(let i = 0; i < myName.length; i++){

	// If the character of your name is a vowel letter, instead of displaying the character, display "*"
    // The ".toLowerCase()" function/method will change the current letter being evaluated in a loop to a lowercase letter ensuring that the letters provided in the expressions below will match

	if(
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" || 
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u" 
		){

		// If the letter in the name is a vowel, it will print the *
		console.log("*");
	}else {
		// Print in the console all non-vowel characters in the name
		console.log(myName[i]);
	}
}


for (let count = 0; count <= 20; count++){
	if(count %2 === 0){

		continue;
	}

	console.log("Continue and Break: " + count)

	if(count > 10){
		break;
	}
}

let name = "alejandro";
for(let i = 0; i < name.length; i++){

	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the next iteration");
		continue;
	}

	if(name[i] == "d"){
		break;
	}
}

