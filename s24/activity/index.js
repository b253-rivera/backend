// Exponent Operator

const getCube = 2 ** 3
console.log(`The cube of 2 is ${getCube}`);

// Template Literals


// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];

const [houseNumber, street, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${street} ${state} ${zipCode}`);


// Object Destructuring

const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};


const {name, species, weight, measurement} = animal;


console.log(`${name} was a ${species}. He weighed at ${weight} with a   measurement of ${measurement}`);


// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((num) => {
 	console.log(num);
});

let reduceNumber = numbers.reduce((num1, num2) => {
	return num1 + num2;
});

console.log(`${reduceNumber}`);



// Javascript Classes

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog();

myDog.name = "Frankie"
myDog.age = 5
myDog.breed = "Miniature Dashshund"

console.log(myDog);

//Do not modify
//For exporting to test.js
try {
	module.exports = {
		getCube,
		houseNumber,
		street,
		state,
		zipCode,
		name,
		species,
		weight,
		measurement,
		reduceNumber,
		Dog
	}	
} catch (err){

}