

/* ES6 UPDATES */

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// Template Literals
/*
	- Allows to write strings without using the concatenation operator (+)
	- Greatly helps with code readability
*/

let name = "John";

// Pre-template literal
let message = "Hello " + name + "! Welcome to programming";
console.log("Message without template literals: " + message)

// Strings using Template Literals
// Uses backticks (``)

message = `Hello ${name}! Welcome to programming`;
console.log(`Message with template literals:  ${message}`);

// Multi-line using Template Literals
const anotherMessage = `
${name} attended a math competition. He won it by solving the problem 8 ** 2 with the solution of ${firstNum}.`


console.log(anotherMessage);

/*
	- Template literals allow us to write strings with embedded JavaScript expressions
	- expressions are any valid unit of code that resolves to a value
	- "${}" are used to include JavaScript expressions in strings using template literals
*/

const interestRate = 0.1;
const principal = 1000;

console.log(`The interest on your savings is: ${principal * interestRate}`);

// Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability

		Syntax: 
			let/const [variableName1, variableName2, variableName3] = arrayName;
*/

const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]} It's nice to meet you!`);

// Array Destructuring
const [firstName, middleName, lastName] = fullName;

console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you!`)

console.log(`Hello ${fullName}! It's nice to meet you!`)

// Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects

	Syntax:
		let/const {propertyName, propertyName, propertyName} = objectName;
*/

const person = {
	givenName: "Juana",
	maidenName: "Dela",
	familyName: "Cruz"
};

// Pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again`);

// Object Destructuring
const {given, maiden, family} = person;

console.log(given);
console.log(maiden);
console.log(family);

console.log(`Hello ${given} ${maiden} ${family}! It's good to see you again`);



/*
	Passing the values using destructured properties of the object
	 Syntax:
	 function funcName ({propertyName, propertyName, propertyName}){
			return statement(parameters)
	 }

	 funcName(object)
*/
// function destructuring
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}


getFullName(person)
//=================================OR=======================
function getFull(givenName, maidenName, familyName){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFull(person.givenName, person.maidenName, person.familyName)

// Arrow Functions
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

// Pre-Arrow Function
// function printFullName(firstName, lastName){
// 	console.log(`${firstName} ${lastName}`);
// }

// printFullName("John", "Smith");

// Arrow Function (=>)

/*
	Syntax:
		let/const variableName = (parameterA, parameterB, parameterC){
				statement
		}

		or (if single-line)

		let/const variableName = () => statement;
*/
const printFullName = (firstName, lastName) => {
	console.log(`${firstName} ${lastName}`);
}

printFullName("Jane", "Smith")

const hello = () => {
	return "hello";
}

hello()

const sum = (x,y) => x + y

let total = sum(1,1);
console.log(total);

person.talk = () => "Hellooooo!";

console.log(person.talk());

// Function with default Argument Value
const greet = (name = "User") => {
	return `Good morning, ${name}!`
};

console.log(greet());
console.log(greet("Ariana"));

// Class-Based Object Blueprints
/*

	Allows the creation/instantation of objects using classes as blueprints

	Syntax:
		class className {
			constructor (objectPropertyA, objectPropertyB){
				this.objectPropertyA = objectPropertyA;
				this.objectPropertyA = objectPropertyA;
			}
		};
*/

class Car {
	constructor(brand, model, year){
		this.brand = brand;
		this.model = model;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

// Values of properties may be assigned after creation/instantation of an object
myCar.brand = "Ford"
myCar.model = "Ranger Raptor"
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);