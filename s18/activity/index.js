// console.log("Hello World!");

function addNum(num1, num2){

	let sum = num1 + num2

	console.log("Displayed sum of 5 and 15 ");
	console.log(sum);

}

addNum(5,15);

//=====================================
function subNum(num3, num4){
	let difference = num3 - num4;

	console.log("Displayed difference of 20 and 5 ");
	console.log(difference);
}

subNum(20,5);

//======================================
function multiplyNum(num5, num6){

	let product = num5 * num6;
	return product;

}

let multiply = multiplyNum(50,10);
console.log("The product of 50 and 10: ");
console.log(multiply);

//======================================
function divideNum(num7, num8){

	let quotient = num7 / num8;
	return quotient;

}

let divide = divideNum(50,10);
console.log("The quotient of 50 and 10: ");
console.log(divide);

//======================================
function getCircleArea(num9){

	let area = 3.1416 * num9 ** 2;
	return area;

}

let circleArea = getCircleArea(15);
console.log("The result of getting the area of a circle with 15 radius ");
console.log(circleArea);
//======================================

function getAverage (num10, num11, num12, num13){

	let average = (num10 + num11 + num12 + num13) / 4
	return average;

}

let averageVar = getAverage(20,40,60,80);
console.log("The average of 20,40,60 and 80:");
console.log(averageVar);
//======================================

function checkIfPassed(num14, num15){

	let score = (num14 / num15) * 100
	return score;

}

let isPassed = 75 < checkIfPassed(38,50) 
console.log("Is 38/50 a passing score?")
console.log(isPassed);













//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}