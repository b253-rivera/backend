// console.log("Hello!");

//Functions
	
		//Parameters and Arguments

			// Functions in javascript are lines/blocks of codes that tell our device/application to perform a certain task when called/invoked
			// Functions are mostly created to create complicated tasks to run several lines of code in succession
			// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

			//We also learned in the previous session that we can gather data from user input using a prompt() window.

function printInput(){
	let nickname = prompt("Enter your nickname");
	console.log("Hi, " + nickname);
}

// printInput();

function printName(name){
	console.log("My name is " + name);
};

printName("Emvir Rivera");
printName("Enma Ai");
printName("Kudo Shinichi");

// variables can also be passed as an argument
let sampleName = "Yuri";

printName(sampleName);

/*

1. Check the divisibility by 8 of 64 and 28
2. if true or false

*/

// Personal Answer
function printResult(result){
	console.log(result % 8 === 0);
}

printResult(64)
printResult(28)

//Correct possible answer

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of " + num + "divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);
//========================================

// Functions as arguments
function argumentFunction(){
	console.log("This function was passed as an argument before the message is printed.");
};

function invokeFunction(argumentFunction){
	argumentFunction();
}
invokeFunction(argumentFunction);

//===========================================

// Multiple Parameters

function createFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Juan", "Dela", "Cruz");
createFullName("Juan", "Dela"); //if argument is lesser than the parameters, the last argument that is not included will return as undefined
createFullName("Juan", "Dela", "Cruz", "Hello"); // if argument is more than the parameters, the exceeding argument will not display

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

//The order of the argument is the same to the order of the parameters. The first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.

// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called the function.

function returnFullName(firstName, middleName, lastName){

	return firstName + " " + middleName + " " + lastName;
	console.log("Cute ko");

	// Notice that the console.log() after the return is no longer printed in the console that is because ideally any line/block of code that comes after the return statement is ignored because it ends the function execution.
}

let compleName = returnFullName("Lucy", " ", "Pevensie");
console.log(compleName);

console.log(returnFullName(firstName, middleName, lastName));


//==================================
function returnAddress(city, country){
	let fullAddress = city + "," + country;
	return fullAddress;
}

let address = returnAddress("Shire", "Middle Earth");
console.log(address);



function printPlayerInfo(username, level, jobClass){

	// console.log("Username: " + username);
	// console.log("Level: " + level);
	// console.log("Job: " + jobClass);

	return "Username: " + username + "\n" + "Level: " + level + "\n" + "Job: " + jobClass;
}

let user1 = printPlayerInfo("janicchi", 95, "Rogue");
console.log(user1)