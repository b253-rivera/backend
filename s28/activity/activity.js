db.single.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000.00,
	"description": "A simple room with all the basic necessities",
	"rooms_available": 10,
	"isAvailable": false
});

db.multiple.insertMany([
		{
			name: "double",
			accomodates: 3,
			price: 2000.00,
			description: "A room fit for a small family going on a vacation",
			roomsAvailable: 5,
			isAvailable: false
		},
		{
			name: "queen",
			accomodates: 4,
			price: 4000.00,
			description: "A room with a queen sized bed perfect for a simple getaway",
			roomsAvailable: 15,
			isAvailable: false
		}
	]);

db.multiple.find({name: "double"});

db.multiple.updateOne(
		{
			name: "queen"
		},
		{
			$set: {
				roomsAvailable: 0,
			}
		}
	);
db.multiple.find({name: "queen"});


db.users.deleteMany({
	roomsAvailable: 0
});