// 1. What directive is used by Node.js in loading the modules it needs?

   // Answer = "require"

// 2. What Node.js module contains a method for server creation?

   // Answer = "createServer" method

// 3. What is the method of the http object responsible for creating a server using Node.js?

	// Answer = "const" method (creating variable for http w/c is responsible for creating a createServer stated on question no.2)

// 4. What method of the response object allows us to set status codes and content types?
	
    // Answer = "writeHead" method

// 5. Where will console.log() output its contents when run in Node.js?

	// Answer = "Git Bash"

// 6. What property of the request object contains the address's endpoint?

	// Answer = ".url"