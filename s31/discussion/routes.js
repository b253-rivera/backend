// ctrl + c = stop the server

const http = require("http");

// Create a variable port to store the port number
const port = 4000;

// Creates a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response) => {

	// Accessing the "greeting" route returns a message of "Hello World"
	// "request" is an object that is sent via the client (browser)
	// The "url" property refers to the url or the link in the browser
	if(request.url == "/greeting"){
		response.writeHead(200, { "Content-Type": "text/plain"});
		response.end("Hello again!")
		// Accessing the "homepage" route returns a message of "This is the homepage"
	} else if (request.url == "/homepage"){
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("This is the homepage");
		// All other routes will return a message of "Page not available"
	} else {
		// Set a status code for the response - a 404 means not Found
		response.writeHead(404, { "Content-Type": "text/plain"});
		response.end("Page not available");
	}
});
// uses "server" and "port" variables created
server.listen(port);

// when server is running, console will print the message via terminal
console.log(`Server now accessible at localhost: ${port}.`);

